package com.lilac.priyacoder.alertdialogsample

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener, DialogInterface.OnClickListener {

    private var name: EditText ?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Enter your name")

        name = EditText(this)
        val lp: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)
        name!!.layoutParams = lp

        alert.setView(name)

        alert.setPositiveButton("SAVE",this)
        alert.setNeutralButton("CANCEL",this)

        alert.show()
    }

    override fun onClick(dialog:DialogInterface,which:Int){

        when(which){
            DialogInterface.BUTTON_POSITIVE ->
                    if(name.toString() != ""){
                        greeting.visibility = View.VISIBLE
                        greeting.text = "Hi" + name!!.text.toString()
                        button.setText(R.string.continueOnClick)
                    }else{
                        Toast.makeText(this,"Please enter name to continue",Toast.LENGTH_LONG)
                    }

            DialogInterface.BUTTON_NEUTRAL ->
                    dialog.cancel()
        }
    }
}
